package Injection;

import Annotations.injectField;
import Exceptions.NoBindingFoundException;

import java.lang.reflect.Field;

public class FieldInjector {

    public static void inject(Object objectToInject)
    {
        System.out.println("FieldInjector.inject()");
        for (Field field : objectToInject.getClass().getDeclaredFields())
        {
            if(field.isAnnotationPresent(injectField.class))
            {
                try
                {
                    Object fieldValue = Mapping.getInstance(field.getType());

                    System.out.println("kjki   " + fieldValue);

                    field.setAccessible(true);
                    field.set(objectToInject, fieldValue);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoBindingFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
