package Injection;

import Annotations.Singleton;

import java.util.List;

public class Injector {

    public static <T> T newInstance(Class<?> classType)
    {
        try
        {
            T objectToInject = (T) Class.forName(classType.getName()).newInstance();

            if(classType.isAnnotationPresent(Singleton.class))  //singleton
            {
                return SingletonMapping.getInstance(classType);
            }
            else //not singleton
            {
                //we need do check all the annotations present in the classType and try to do all the injections (field, setter, constructor)

                SetterInjector.inject(objectToInject);
                objectToInject  = ConstructorInjector.inject(objectToInject);
                //FieldInjector.inject(objectToInject);

            }
            return objectToInject;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static <T> T getRequiredImplementation(List<Object> implementations)
    {
        Class<?> classFound = (Class<?>) implementations.get(0);
        System.out.println("implementation found : " + classFound.getName());
        return newInstance(classFound);
    }

}
