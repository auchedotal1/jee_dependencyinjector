package Injection;

import java.util.HashMap;

public class SingletonMapping {

    private static HashMap<Class<?>, Object> singletonMap = new HashMap<Class<?>, Object>();

    public static <T> T getInstance (Class<?> classType)
    {
        Object res = singletonMap.get(classType);

        if(res==null) //singleton first time instantiation
        {
            try
            {
                T singleton = (T) Class.forName(classType.getName()).newInstance();
                singletonMap.put(classType, singleton);
                return singleton;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        else //singleton was already created
        {
            return (T) res;
        }
        return null;
    }
}
