package Injection;

import Exceptions.NoBindingFoundException;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Mapping {

    private static HashMap<Class<?>, List<Object>> classMap = new HashMap<Class<?>, List<Object>>();


    public static <T1, T2> void add(Class<T1> classType, T2 classObject)
    {
        List<Object> classObjects = classMap.get(classType);
        if(classObjects == null)
        {
            classObjects = new ArrayList<Object>();
            classObjects.add(classObject);
            classMap.put(classType, classObjects);
        }
        else
        {
            if(!classObjects.contains(classObject))
            {
                classObjects.add(classObject);
            }
        }
    }

    public static <T> T getInstance(Class<T> classType) throws NoBindingFoundException {
        System.out.println("Mapping.getIntance() : " + classType.getSimpleName());

        if (classType.isInterface()) //interface
        {
            System.out.println("Mapping.getIntance() : Interface Type");
            List<Object> allImplementations = classMap.get(classType);
            if(allImplementations != null)
            {
                return Injector.getRequiredImplementation(allImplementations);
            }
            else
                throw new NoBindingFoundException();
        }
        else //class
        {
            if (classType.isPrimitive()) //value type
            {
                System.out.println("Mapping.getIntance() : Primitive Type");
                Object obj = (T)classMap.get(classType).get(0);
                if (obj != null)
                    return (T) obj;
                else
                    throw new NoBindingFoundException();
            }
            else //custom type
            {
                if(Modifier.isAbstract(classType.getModifiers())) //abstract class type
                {
                    System.out.println("Mapping.getIntance() : abstract class");
                    return null;
                }
                else
                {
                    System.out.println("Mapping.getIntance() : Custom Type");
                    return Injector.newInstance(classType);
                }
            }
        }
    }

    public static void clear()
    {
        classMap.clear();
    }
}