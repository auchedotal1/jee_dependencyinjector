package Injection;

import Annotations.injectSetter;
import Exceptions.NoBindingFoundException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class SetterInjector {

    static void inject(Object objectToInject)
    {
        System.out.println("SetterInjector.inject()");
        if(objectToInject != null)
        {
            //we get through all setters and check if an inject annotation is present
            for (Method setter : getSetters(objectToInject.getClass()))
            {
                if (setter.isAnnotationPresent(injectSetter.class))
                {
                    if(fieldNeedInjection(objectToInject, setter))
                    {
                        try
                        {
                            Class<?> setterType = setter.getParameterTypes()[0];
                            Object field = Mapping.getInstance(setterType);
                            setter.invoke(objectToInject, field);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoBindingFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    //checks if method is a setter
    private static boolean isSetter(Method method){
        if(!method.getName().startsWith("set")) return false;
        if(method.getParameterTypes().length != 1) return false;
        return true;
    }

    //get all the setters for a given class type
    private static List<Method> getSetters(Class<?> classType)
    {
        ArrayList<Method> setters = new ArrayList<Method>();
        for(Method method : classType.getDeclaredMethods())
        {
            if (isSetter(method))
                setters.add(method);
        }
        return setters;
    }

    private static boolean fieldNeedInjection (Object objectToInject, Method setter)
    {
        //name of the field associated to the setter considering it's the same in lower case
        String fieldName = setter.getName().toLowerCase().replace("set", "");
        try
        {
            Field field = objectToInject.getClass().getDeclaredField(fieldName);
            if(field != null)
            {
                field.setAccessible(true);
                //if field type is primitive or field has not yet been instantiated
                return (field.getClass().isPrimitive() || field.get(objectToInject) == null);
            }

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }
}
