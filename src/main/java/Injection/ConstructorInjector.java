package Injection;

import Annotations.injectConstructor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class ConstructorInjector {

    public static <T> T inject (Object objectToInject)
    {
        System.out.println("ConstructorInjector.inject()");

        if (objectToInject != null)
        {
            try
            {
                List<Constructor> constructors = getConstructors(objectToInject.getClass());
                if(constructors.size() > 0)
                {
                    Constructor constructor = constructors.get(0);

                    Class<?>[] parameters = constructor.getParameterTypes();
                    Object[] injectParameters = new Object[parameters.length];

                    int i = 0;
                    for(Class<?> param : parameters)
                    {
                        if(fieldNeedInjection(objectToInject, parameters[i]))
                        {
                            System.out.println("creation of an instance of " + parameters[i]);
                            Object obj = Injector.newInstance(parameters[i]);
                            injectParameters[i] = obj;
                        }
                        else
                        {
                            Field field = objectToInject.getClass().getDeclaredField(getParamName(parameters[i]));
                            // Change visibility to access to its implementation
                            field.setAccessible(true);
                            Object attribute = field.get(objectToInject);
                            injectParameters[i] = attribute;
                        }
                        i++;
                    }
                    objectToInject = constructor.newInstance(injectParameters);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        return (T) objectToInject;
    }

    public static ArrayList<Constructor> getConstructors (Class<?> classType)
    {
        ArrayList<Constructor> resConstructors = new ArrayList<Constructor>();
        for (Constructor<?> constructor : classType.getDeclaredConstructors())
        {
            if (constructor.isAnnotationPresent(injectConstructor.class))
            {
                resConstructors.add(constructor);
            }
        }
        return resConstructors;
    }

    private static String getParamName(Class<?> param)
    {
        String[] fieldNameWords = param.getName().split(".");
        return fieldNameWords[fieldNameWords.length-1].toLowerCase();
    }

    private static boolean fieldNeedInjection (Object objectToInject, Class<?> param)
    {
        String fieldName = getParamName(param);
        try
        {
            Field field = objectToInject.getClass().getDeclaredField(fieldName);
            if(field != null)
            {
                field.setAccessible(true);
                //if field type is primitive or field has not yet been instantiated
                return (param.isPrimitive() || field.get(objectToInject) == null);
            }

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

}
