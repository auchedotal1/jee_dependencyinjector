package Tests;

import Injection.Mapping;
import TestClasses.TestFieldsInject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class testsOnFieldInjection {

    @Test
    public void testSetterInjection ()
    {
        try
        {
            Mapping.add(Integer.class, 2);

            TestFieldsInject testClass = Mapping.getInstance(TestFieldsInject.class);

            assertEquals(testClass.getNb(), 2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
