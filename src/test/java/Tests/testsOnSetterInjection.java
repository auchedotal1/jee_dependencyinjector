package Tests;

import Injection.Mapping;
import TestClasses.FileToObject;
import TestClasses.JsonParser;
import TestInterfaces.FileToObjectInterface;
import TestInterfaces.JsonParserInterface;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class testsOnSetterInjection {

    @Test
    public void testSetterInjectionA ()
    {
        try
        {
            Mapping.add(JsonParserInterface.class, JsonParser.class);
            Mapping.add(FileToObjectInterface.class, FileToObject.class);
            Mapping.add(boolean.class,true);
            Mapping.add(String.class,"abc");

            FileToObjectInterface fileToObj = Mapping.getInstance(FileToObjectInterface.class);

            assertEquals(fileToObj.getBool(), true);
            //assertEquals(fileToObj.getString(), "abc");
            //assertTrue(fileToObj.getJsonparser() instanceof JsonParser);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    @Test
    public void testSetterInjectionB ()
    {
        try
        {
            Mapping.clear();
            Mapping.add(JsonParserInterface.class, JsonParser.class);
            Mapping.add(FileToObjectInterface.class, FileToObject.class);
            Mapping.add(boolean.class,true);
            Mapping.add(String.class,"abc");

            FileToObjectInterface fileToObj = Mapping.getInstance(FileToObjectInterface.class);

            //assertEquals(fileToObj.getBool(), true);
            assertEquals(fileToObj.getString(), "abc");
            //assertTrue(fileToObj.getJsonparser() instanceof JsonParser);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    @Test
    public void testSetterInjectionC ()
    {
        try
        {
            Mapping.clear();
            Mapping.add(JsonParserInterface.class, JsonParser.class);
            Mapping.add(FileToObjectInterface.class, FileToObject.class);
            Mapping.add(boolean.class,true);
            Mapping.add(String.class,"abc");

            FileToObjectInterface fileToObj = Mapping.getInstance(FileToObjectInterface.class);

            //assertEquals(fileToObj.getBool(), true);
            //assertEquals(fileToObj.getString(), "abc");
            assertTrue(fileToObj.getJsonparser() instanceof JsonParser);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
