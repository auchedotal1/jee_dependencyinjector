package Tests;

import Injection.Mapping;
import TestClasses.EmptyClass;
import TestInterfaces.EmptyClassInterface;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class testsOnEmptyObjects {

    @Test
    public void testSetterInjection ()
    {
        try
        {
            Mapping.add(EmptyClassInterface.class, EmptyClass.class);

            String temp2;

            EmptyClassInterface emptyClass = Mapping.getInstance(EmptyClassInterface.class);

            String temp;

            assertTrue(emptyClass instanceof EmptyClassInterface);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
