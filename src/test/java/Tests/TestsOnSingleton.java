package Tests;

import Injection.Mapping;
import TestClasses.SingletonClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestsOnSingleton {

    @Test
    public void testUniqueSingleton ()
    {
        try
        {
            SingletonClass singletonA = Mapping.getInstance(SingletonClass.class);
            SingletonClass singletonB = Mapping.getInstance(SingletonClass.class);

            assertEquals(singletonA, singletonB);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


}
