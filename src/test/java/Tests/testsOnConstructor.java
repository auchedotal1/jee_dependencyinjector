package Tests;

import Injection.Mapping;
import TestClasses.ConstructorTestClass;
import TestClasses.JsonParser;
import TestInterfaces.ConstructorTestInterface;
import TestInterfaces.JsonParserInterface;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class testsOnConstructor {

    @Test
    public void testInjectConstructor ()
    {
        try
        {
            Mapping.add(ConstructorTestInterface.class, ConstructorTestClass.class);
            Mapping.add(JsonParserInterface.class, JsonParser.class);
            Mapping.add(int.class, 1);

            ConstructorTestClass testClass = Mapping.getInstance(ConstructorTestClass.class);

            assertTrue(testClass.getParser() instanceof JsonParser);
            assertEquals(testClass.getNb(), 1);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
