package TestClasses;

import Annotations.injectConstructor;
import Injection.ConstructorInjector;
import TestInterfaces.ConstructorTestInterface;
import TestInterfaces.JsonParserInterface;

public class ConstructorTestClass implements ConstructorTestInterface {
    private JsonParserInterface parser;
    private int nb;

    @injectConstructor
    public ConstructorTestClass(JsonParserInterface json, int n)
    {
        this.parser = json;
        this.nb = n;
    }

    public JsonParserInterface getParser() {return parser;}
    public int getNb() {return nb;}
}
