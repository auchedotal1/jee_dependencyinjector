package TestClasses;

import Annotations.injectField;

public class TestFieldsInject {

    @injectField(value = "2")
    private int nb;

    public int getNb() {
        return nb;
    }
}
