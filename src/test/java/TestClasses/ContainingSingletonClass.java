package TestClasses;

import Annotations.injectField;

public class ContainingSingletonClass {

    @injectField
    SingletonClass singleton;

    public SingletonClass getSingleton() {return singleton;}
}
