package TestClasses;

import Annotations.injectSetter;
import TestInterfaces.FileToObjectInterface;
import TestInterfaces.JsonParserInterface;

public class FileToObject implements FileToObjectInterface {

    private boolean bool;
    private String string;
    private JsonParserInterface jsonparser;

    @injectSetter
    public void setBool(boolean b) {bool=b;}
    @injectSetter
    public void setString(String s) {string = s;}
    @injectSetter
    public void setJsonparser(JsonParserInterface parser) {this.jsonparser=parser;}

    @Override
    public boolean getBool () {return bool;}
    @Override
    public String getString () {return string;}
    @Override
    public JsonParserInterface getJsonparser () {return jsonparser;}


}
