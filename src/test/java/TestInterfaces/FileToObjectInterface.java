package TestInterfaces;

public interface FileToObjectInterface {

    public boolean getBool ();
    public String getString ();
    public JsonParserInterface getJsonparser ();

}
